import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  posts = [
	  	{   
	  		title: "Le titre du premier article sympa",  
	  	    content: "Il faut bien écrire quelque chose pour remplir cet espace. C'est un lorem ipsum inédit qui veut bien être comprise que ce soit avec des mot courts comme Bien ou des mots long comme intergouvernementalisation il s'adapte bien pour remplir les vides.",  
	  	    loveIts: 2,  
	  	    created_at: new Date()
	  	},
	  	{   
	  		title: "Le titre du second, un lorem FR-fr",  
	  	    content: "Il faut bien écrire quelque chose pour remplir cet espace. C'est un lorem ipsum inédit qui veut bien être comprise que ce soit avec des mot courts comme Bien ou des mots long comme intergouvernementalisation il s'adapte bien pour remplir les vides.",  
	  	    loveIts: -3,  
	  	    created_at: new Date()
	  	},
	  	{   
	  		title: "L'article le plus aimé",  
	  	    content: "Il faut bien écrire quelque chose pour remplir cet espace. C'est un lorem ipsum inédit qui veut bien être comprise que ce soit avec des mot courts comme Bien ou des mots long comme intergouvernementalisation il s'adapte bien pour remplir les vides.",  
	  	    loveIts: 4,  
	  	    created_at: new Date()
	  	},
	  	{   
	  		title: "neutre, autant aimé que le contraire",  
	  	    content: "Il faut bien écrire quelque chose pour remplir cet espace. C'est un lorem ipsum inédit qui veut bien être comprise que ce soit avec des mot courts comme Bien ou des mots long comme intergouvernementalisation il s'adapte bien pour remplir les vides.",
	  	    loveIts: 0,  
	  	    created_at: new Date()
	  	},
	  	{   
	  		title: "Ce lorem ipsum n'est pas dolor",  
	  	    content: "Il faut bien écrire quelque chose pour remplir cet espace. C'est un lorem ipsum inédit qui veut bien être comprise que ce soit avec des mot courts comme Bien ou des mots long comme intergouvernementalisation il s'adapte bien pour remplir les vides.",
	  	    loveIts: -1,  
	  	    created_at: new Date()
	  	}
  	];
}
